function isVersion4(){
	if((navigator.appName.indexOf("Microsoft")>-1 && navigator.userAgent.indexOf("MSIE 4")>-1) || (navigator.appName.indexOf("Netscape")>-1 && navigator.userAgent.indexOf("Mozilla/4")>-1)) {
	return true;
}
	return false;
}
function insertBasicNavi(){
	if(isVersion4()) {
	var baseNavi = "| <a href='http://www.smbc.co.jp/'>SMBCトップ</a> | ";
	baseNavi =baseNavi+ "<a href='http://direct.smbc.co.jp/aib/'>インターネットバンキング SMBCダイレクト</a> | ";
	baseNavi =baseNavi+ "<a href='http://www.smbc.co.jp/kojin/tenpo/index.html'>店舗・ATM</a> | ";
	baseNavi =baseNavi+ "<a href='http://www.smbc.co.jp/kojin/fee/index.html'>手数料一覧</a> | ";
	baseNavi =baseNavi+ "<a href='http://www.smbc.co.jp/kojin/kinri/index.html'>金利一覧・外国為替相場</a> | ";
	baseNavi =baseNavi+ "<a href='http://www.smbc.co.jp/kojin/otoiawase/index.html'>お困りの際は</a> | <br>";
	baseNavi =baseNavi+ "| <a href='http://www.smbc.co.jp/kojin/index.html'>個人のお客さま</a> | ";
	baseNavi =baseNavi+ "<a href='http://www.smbc.co.jp/hojin/index.html'>法人のお客さま</a> | ";
	baseNavi =baseNavi+ "<a href='http://www.smbc.co.jp/aboutus/index.html'>三井住友銀行について</a> | ";
	baseNavi =baseNavi+ "<a href='http://www.smbc.co.jp/news/index.html'>ニュースリリース</a> | ";
	baseNavi =baseNavi+ "<a href='http://www.smbc.co.jp/recruit/index.html'>採用情報</a> | ";
	baseNavi = "<p class='fNavi'>" + baseNavi + "</p>"; 
	document.write(baseNavi);
	}
}
function insertOldBrowser(){
	if(isVersion4()) {
	var baseStr = "<p class='oldBrsCaution'><a href='http://www.smbc.co.jp/ln/direct/LinksServlet?id=top1_02I'>→旧バージョンのブラウザ<br />をお使いの方へ</a></p>";
	document.write(baseStr);
	}
}
