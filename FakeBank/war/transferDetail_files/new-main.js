//Tile.js
(function(a){a.fn.tile=function(b){var c,d,e,f,g=this.length-1,h;if(!b)b=this.length;this.each(function(){h=this.style;if(h.removeProperty)h.removeProperty("height");else if(h.removeAttribute)h.removeAttribute("height")});return this.each(function(h){e=h%b;if(e==0)c=[];c[e]=a(this);f=c[e].height();if(e==0||f>d)d=f;if(h==g||e==b-1)a.each(c,function(){this.height(d)})})}})(jQuery)
$(function(){
	$(".js-tile2 > li").tile(2);
	$(".js-tile3 > li").tile(3);
	$(".js-tile4 > li").tile(4);
	$(".tile1").tile();
	$(".tile2").tile();
	$(".tile3").tile();
	$(".tile4").tile();
	$(".tile5").tile();
});
$(function(){
	$(".lt-ie9 .tile1 th, .lt-ie9 .tile1 td").css({'table-layout':'fixed'});
});

//Page Scroll
$(function(){
	 var headH = 50;
	 $('#toTop a').each(function () {
			jQuery.easing.quart = function (x, t, b, c, d) {
				 return -c * ((t = t / d - 1) * t * t * t - 1) + b;
			};
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname && this.hash.replace(/#/, '')) {
				 var $targetId = $(this.hash),
						$targetAnchor = $('[name=' + this.hash.slice(1) + ']');
				 var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
				 if ($target) {
						var targetOffset = $target.offset().top - headH;
						$(this).click(function () {
							 $('html, body').animate({
									scrollTop: targetOffset
							 }, 700, 'quart');
							 return false;
						});
				 }
			}
	 });
	 if (location.hash) {
			$(window).load(function(){
				 var hash = location.hash;
				 var $targetId = $(hash),
						$targetAnchor = $('[name=' + hash.slice(1) + ']');
				 var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
				 if ($target) {
						var targetOffset = $targetId.offset().top - headH;
						$('html, body').animate({
									scrollTop: targetOffset
							 }, 500, 'quart');
				 }
			});
	 }
});

//Navigation
$(function(){
	var suvNav = $('#suvNav');
	var sunNavBtn = $('.glNavBtn a');
	var closeBtn = $('.closeBtn');
	var docHeight = $(document).height();

	$(window).on("load resize", function(){
		var docWidth = $(document).width() - 20;
	});
	var selectHide = $('#header').outerHeight() + suvNav.outerHeight();

	var slideFunc = function(){
		suvNav.slideToggle(350, 'swing');
		sunNavBtn.toggleClass('open');
		$('#header').toggleClass('open')
		$('#suvWrap').toggleClass('covBody')
		closeBtn.toggleClass('showBtn');

		$('.lt-ie7 select').each(function(){
			if($(this).css('visibility') == 'hidden') {
				$(this).css({'visibility':'visible'});
			} else {
				var selectHeight = $(this).offset().top;
				if(selectHide > selectHeight){
					$(this).css({'visibility':'hidden'});
				}
			}
		});

	};

	suvNav.appendTo('#header').hide();
	$('#header').after('<div id="suvWrap"></div>')

	var suvWrap = $('#suvWrap')
	$('.lt-ie7 #suvWrap').css({'height':docHeight + 'px'});

	sunNavBtn.on('click',function(){
		slideFunc();
		return false;
	});

	suvWrap.on('click', function(){
		slideFunc();
		return false;
	});

	closeBtn.appendTo('#suvNav .wrapper').on('click', function(){
		slideFunc();
		return false;
	});

})

//Go to top
$(function(){

	 var pageTop = $("#toTop a");
	 var pageTopFix = $("#Footer").offset().top - $(window).height();

	 $(window).scroll(function(){
			if($(window).scrollTop() < 100){
				 pageTop.hide();
			} else if($(window).scrollTop() < pageTopFix){
				 pageTop.show().addClass("fixed");
			} else {
				 pageTop.removeClass("fixed");
			}
	 });
});

//Edit Btn
$(function(){
	$('.editName, .editTel').on('click' , function(){
		var pA = $(this).parent();
		pA.children('.editData').show().attr('value',pA.children('.itemTxt').text());
		pA.children('.itemTxt').hide();
		pA.children('.js-offTxt').show();
	});
});


$(function(){
//Table odd line
	$('.js-even tr:even').addClass('evenLine');

//Safari font
	if(jQuery.browser.safari){
		$("body").addClass('safari');
	}
//Tablet
	var ua = navigator.userAgent;
	if((ua.indexOf('iPad')>0)||(ua.indexOf('Android')>0) ){
		$("body").addClass('tablet');
	}
//ipad
	if((ua.indexOf('iPad')>0)){
		$("body").addClass('ipad');
	}
});

//IE7 Glid
$(function(){
	$('.lt-ie7 .glid > li:first-child').addClass("firstChild");
});

//IE7 RingNote
$(function(){
	if(0 < $('link[href="/aib/css/new-ring.css"]').size()){
		$('.lt-ie8 #contWrap').before('<div class="ringTop"></div>').after('<div class="ringBtm"></div>');
    $('meta[name=viewport]').attr('content','width=1000');
	}
});
