var keyidArray = [];
var keystartArray = [];
var keypressArray = [];
var keynextArray = [];

var pastpressArray = [];
var pastnextArray = [];
var pastUPressArray = [];
var pastUNextArray = [];
var pastPressAve;
var pastNextAve;

var focuscount = 0;
window.onfocus = function(){
	//alert("focus");
	focuscount ++;
	test.value = focuscount;
};

window.addEventListener('keydown', keydownEvent, false);

function keydownEvent(e){
	var nowdate = Date.now();
	if(keystartArray.length > 0){
		var next = nowdate - keystartArray[keystartArray.length -1];
		keynextArray.push(next);
	}
	keystartArray.push(nowdate);
	var id = e.keyCode;
	keyidArray.push(id);
	keypressArray.push(0);
	//alert(e.keyCode);
}


window.addEventListener('keyup', keyupEvent, false);

function keyupEvent(e){
	var nowdate = Date.now();
	for(var i = keyidArray.length -1 ; i >= 0 ; i --){
		if(keyidArray[i] == e.keyCode){
			var presstime = nowdate - keystartArray[i];
			keypressArray[i] = presstime;
			break;
		}
	}
}

function generateTypingData_2(){
	keynextArray.push(0);
	
	var pressAll = 0;
	for(var i = 0 ; i < keypressArray.length ; i ++){
		pressAll += pastpressArray[i];
	}
	var nextAll = 0;
	for(var i = 0 ; i < keynextArray.length -1 ; i ++){
		nextAll += keynextArray[i];
	}
	var pressAve = pressAll / keypressArray.length;
	var nextAve = nextAll / (keynextArray.length -1);
	
	var keyUPressArray = [];
	for(var i = 0 ; i < keypressArray.length ; i ++){
		if(keypressArray[i] > pressAve * 1.3){
			keyUPressArray.push(keypressArray[i]);
		}else{
			keyUPressArray.push(0);
		}
	}
	var keyUNextArray = [];
	for(var i = 0 ; i < keynextArray.length ; i ++){
		if(keynextArray[i] > nextAve * 1.3){
			keyUNextArray.push(keynextArray[i]);
		}else{
			keyUNextArray.push(0);
		}
	}
	
	if(pastpressArray.length == 0 || pastpressArray.length != keypressArray.length){
		/*pastpressArray = keypressArray;
		pastnextArray = keynextArray;
		pastUPressArray = keyUPressArray;
		pastUNextArray = keyUNextArray;*/
		keyidArray = [];
		keystartArray = [];
		keypressArray = [];
		keynextArray = [];
		return;
	}

	var uniqueAll = 0;
	var uniqueCount = 0;
	var str = "P  ";
	for(var i = 0 ; i < keyUPressArray.length ; i ++){
		if(keyUPressArray[i] != 0 && pastUPressArray[i] != 0){
			if(keyUPressArray[i] +20 >= pastUPressArray[i] && keyUPressArray[i] -20 <= pastUPressArray[i]){
				uniqueAll += 0.9999;
				str += i + " : " + keyUPressArray[i] + " ";
			}else if(keyUPressArray[i] > pastUPressArray[i]){
				var n = (pastUPressArray[i] +20) / keyUPressArray[i];
				uniqueAll += n;// * (((pastUPressArray[i] +10 +keyUPressArray[i]) / 2) / keyUPressArray[i]);
			}else{
				var n = (keyUPressArray[i] +20) / pastUPressArray[i];
				uniqueAll += n;// * (((keyUPressArray[i] +10 +pastUPressArray[i]) / 2) / pastUPressArray[i]);
			}
		}else if(keyUPressArray[i] != 0){
			if(keyUPressArray[i] +20 >= pastpressArray[i] && keyUPressArray[i] -20 <= pastpressArray[i]){
				uniqueAll += 0.9999;
				str += i + " : " + keyUPressArray[i] + " ";
			}else if(keyUPressArray[i] > pastpressArray[i]){
				var n = (pastpressArray[i] +20) / keyUPressArray[i];
				uniqueAll += n * (((pastpressArray[i] +20 +keyUPressArray[i]) / 2) / keyUPressArray[i]);
				//alert(n * ((n + keyUPressArray[i]) / 2));
			}else{
				var n = (keyUPressArray[i] +20) / pastpressArray[i];
				uniqueAll += n * (((keyUPressArray[i] +20 +pastpressArray[i]) / 2) / pastpressArray[i]);
				//alert(n * ((n + pastpressArray[i]) / 2));
			}
		}else if(pastUPressArray[i] != 0){
			if(keypressArray[i] +20 >= pastUPressArray[i] && keypressArray[i] -20 <= pastUPressArray[i]){
				uniqueAll += 0.9999;
				str += i + " : " + keypressArray[i] + " ";
			}else if(keypressArray[i] > pastUPressArray[i]){
				var n = (pastUPressArray[i] +20) / keypressArray[i];
				uniqueAll += n * (((pastUPressArray[i] +20 +keypressArray[i]) / 2) / keypressArray[i]);
			}else{
				var n = (keypressArray[i] +20) / pastUPressArray[i];
				uniqueAll += n * (((keypressArray[i] +20 +pastUPressArray[i]) / 2) / pastUPressArray[i]);
			}
		}else{
			continue;
		}
		
		uniqueCount ++;
	}
	
	str += "\nN  ";
	for(var i = 0 ; i < keyUNextArray.length ; i ++){
		if(keyUNextArray[i] != 0 && pastUNextArray[i] != 0){
			if(keyUNextArray[i] +10 >= pastUNextArray[i] && keyUNextArray[i] -10 <= pastUNextArray[i]){
				uniqueAll += 0.9999;
				str += i + " : " + keyUNextArray[i] + " ";
			}else if(keyUNextArray[i] > pastUNextArray[i]){
				var n = (pastUNextArray[i] +10) / keyUNextArray[i];
				uniqueAll += n;//n * (((pastUNextArray[i] +10 +keyUNextArray[i]) / 2) / keyUNextArray[i]);
			}else{
				var n = (keyUNextArray[i] +10) / pastUNextArray[i];
				uniqueAll += n;// * (((keyUNextArray[i] +10 +pastUNextArray[i]) / 2) / pastUNextArray[i]);
			}
		}else if(keyUNextArray[i] != 0){
			if(keyUNextArray[i] +10 >= pastnextArray[i] && keyUNextArray[i] -10 <= pastnextArray[i]){
				uniqueAll += 0.9999;
				str += i + " : " + keyUNextArray[i] + " ";
			}else if(keyUNextArray[i] > pastnextArray[i]){
				var n = (pastnextArray[i] +10) / keyUNextArray[i];
				uniqueAll += n * (((pastnextArray[i] +10 +keyUNextArray[i]) / 2) / keyUNextArray[i]);
			}else{
				var n = (keyUNextArray[i] +10) / pastnextArray[i];
				uniqueAll += n * (((keyUNextArray[i] +10 +pastnextArray[i]) / 2) / pastnextArray[i]);
			}
		}else if(pastUNextArray[i] != 0){
			if(keynextArray[i] +10 >= pastUNextArray[i] && keynextArray[i] -10 <= pastUNextArray[i]){
				uniqueAll += 0.9999;
				str += i + " : " + keynextArray[i] + " ";
			}else if(keynextArray[i] > pastUNextArray[i]){
				var n = (pastUNextArray[i] +10) / keynextArray[i];
				uniqueAll += n * (((pastUNextArray[i] +10 +keynextArray[i]) / 2) / keynextArray[i]);
			}else{
				var n = (keynextArray[i] +10) / pastUNextArray[i];
				uniqueAll += n * (((keynextArray[i] +10 +pastUNextArray[i]) / 2) / pastUNextArray[i]);
			}
		}else{
			continue;
		}
		uniqueCount ++;
	}
	
	var nearP;
	if(pastPressAve +5 >= pressAve && pastPressAve -5 <= pressAve){
		nearP = 1;
	}else if(pastPressAve > pressAve){
		var n = (pressAve +5) / pastPressAve;
		nearP = n;
	}else{
		var n = (pastPressAve +5) / pressAve;
		nearP = n;
	}
	//alert(nearP);
	
	var nearN;
	if(pastNextAve +5 >= nextAve && pastNextAve -5 <= nextAve){
		nearN = 1;
	}else if(pastNextAve > nextAve){
		var n = (nextAve +5) / pastNextAve;
		nearN = n;
	}else{
		var n = (pastNextAve +5) / nextAve;
		nearN = n;
	}
	//alert(nearN);
	
	uniqueAll += Math.pow(nearP, 2);
	uniqueAll += Math.pow(nearN, 2);
	uniqueCount += 2;
	
	var uniqueAve;
	if(uniqueCount > 0){
		uniqueAve = uniqueAll / uniqueCount;
	}else{
		uniqueAve = 0;
	}
	
	var tr = document.createElement("tr");
	var result = document.createElement("td");
	tr.appendChild(result);
	result.innerHTML = (uniqueAve.toFixed(2) * 100) + "%";
	result.style.width = "50px";
	result.style.textAlign = "right";
	result.style.border = "solid 1px #cccccc";
	
	for(var i = 0 ; i < keyidArray.length ; i ++){
		var dummy = document.createElement("td");
		tr.appendChild(dummy);
		dummy.style.width = "10px";
		
		var press = document.createElement("td");
		tr.appendChild(press);
		press.innerHTML = keypressArray[i];
		press.style.width = "35px";
		press.style.textAlign = "right";
		if(keyUPressArray[i] != 0){
			press.style.color = "#ff0000";
		}
		press.style.border = "solid 1px #cccccc";
		
		var nex = document.createElement("td");
		tr.appendChild(nex);
		nex.style.width = "35px";
		nex.style.textAlign = "right";
		nex.innerHTML = keynextArray[i];
		if(keyUNextArray[i] != 0){
			nex.style.color = "#0000ff";
		}
		nex.style.border = "solid 1px #cccccc";
	}
	keydata.appendChild(tr);
	
	//alert(uniqueAve);
	
	/*pastpressArray = keypressArray;
	pastnextArray = keynextArray;
	pastUPressArray = keyUPressArray;
	pastUNextArray = keyUNextArray;*/
	keyidArray = [];
	keystartArray = [];
	keypressArray = [];
	keynextArray = [];
}

function recordTypingData(){
	keynextArray.push(0);
	
	var pressAll = 0;
	for(var i = 0 ; i < keypressArray.length ; i ++){
		pressAll += keypressArray[i];
	}
	var nextAll = 0;
	for(var i = 0 ; i < keynextArray.length -1 ; i ++){
		nextAll += keynextArray[i];
	}
	var pressAve = pressAll / keypressArray.length;
	var nextAve = nextAll / (keynextArray.length -1);
	
	var keyUPressArray = [];
	for(var i = 0 ; i < keypressArray.length ; i ++){
		if(keypressArray[i] > pressAve * 1.3){
			keyUPressArray.push(keypressArray[i]);
		}else{
		//	alert(keypressArray[i] + " : " + (pressAve) );
			keyUPressArray.push(0);
		}
	}
	var keyUNextArray = [];
	for(var i = 0 ; i < keynextArray.length ; i ++){
		if(keynextArray[i] > nextAve * 1.3){
			keyUNextArray.push(keynextArray[i]);
		}else{
			keyUNextArray.push(0);
		}
	}
	
	
	while(recorddata.childNodes.length > 0){
		recorddata.removeChild(recorddata.childNodes[0]);
	}
	while(keydata.childNodes.length > 0){
		keydata.removeChild(keydata.childNodes[0]);
	}
	var tr = document.createElement("tr");
	var resultdummy = document.createElement("td");
	tr.appendChild(resultdummy);
	resultdummy.style.width = "50px";
	resultdummy.style.textAlign = "right";
	for(var i = 0 ; i < keyidArray.length ; i ++){
		var dummy = document.createElement("td");
		tr.appendChild(dummy);
		dummy.style.width = "10px";
		
		var press = document.createElement("td");
		tr.appendChild(press);
		press.innerHTML = keypressArray[i];
		press.style.width = "35px";
		press.style.textAlign = "right";
		if(keyUPressArray[i] != 0){
			press.style.color = "#ff0000";
		}
		press.style.border = "solid 1px #cccccc";
		
		var nex = document.createElement("td");
		tr.appendChild(nex);
		nex.style.width = "35px";
		nex.style.textAlign = "right";
		nex.innerHTML = keynextArray[i];
		if(keyUNextArray[i] != 0){
			nex.style.color = "#0000ff";
		}
		nex.style.border = "solid 1px #cccccc";
	}
	recorddata.appendChild(tr);

	pastpressArray = keypressArray;
	pastnextArray = keynextArray;
	pastUPressArray = keyUPressArray;
	pastUNextArray = keyUNextArray;
	pastPressAve = pressAve;
	pastNextAve = nextAve;
	keyidArray = [];
	keystartArray = [];
	keypressArray = [];
	keynextArray = [];
}











function generateTypingData(){
	keynextArray.push(0);
	var div = document.createElement("div");
	for(var i = 0 ; i < keyidArray.length ; i ++){
		
		var press = document.createElement("span");
		div.appendChild(press);
		press.innerHTML = keypressArray[i];
		press.style.marginLeft = "20px";
		
		var nex = document.createElement("span");
		div.appendChild(nex);
		nex.style.marginLeft = "10px";
		nex.innerHTML = keynextArray[i];
	}
	keydata.appendChild(div);
	
	//alert(keyidArray.length);
	
	var most = 0;
	var all = 0;
	var pressAll = 0;
	var nextAll = 0;
	if(pastpressArray.length > 0 && pastpressArray.length == keypressArray.length){
		//alert("start");
		for(var i = 0 ; i < keypressArray.length ; i ++){
			if(keypressArray[i] > most){
				most = keypressArray[i];
			}
			
			var near;
			if(keypressArray[i] >= pastpressArray[i]){
				if(pastpressArray[i] + 10 >= keypressArray[i]){
					near = 0.999;
				}else{
					near = (pastpressArray[i] + 10) / keypressArray[i];
				}
			}else{
				if(keypressArray[i] + 10 >= pastpressArray[i]){
					near = 0.999;
				}else{
					near = (keypressArray[i] + 10) / pastpressArray[i];
				}
			}
			all += near;
			pressAll += pastpressArray[i];
		}
		for(var i = 0 ; i < keynextArray.length -1 ; i ++){
			if(keynextArray[i] > most){
				most = keynextArray[i];
			}
			
			var near;
			if(keynextArray[i] >= pastnextArray[i]){
				if(pastnextArray[i] + 10 >= keynextArray[i]){
					near = 0.999;
				}else{
					near = (pastnextArray[i] +10) / keynextArray[i];
				}
			}else{
				if(keynextArray[i] + 10 >= pastnextArray[i]){
					near = 0.999;
				}else{
					near = (keynextArray[i] +10) / pastnextArray[i];
				}
			}
			all += near;
			nextAll += keynextArray[i];
		}
		var ave = all / (keypressArray.length + keynextArray.length -1) * 100;
		
		var pressAve = pressAll / keypressArray.length;
		var nextAve = nextAll / (keynextArray.length -1);
		
		var str = "\nP  ";
		for(var i = 0 ; i < keypressArray.length ; i ++){
			if(keypressArray[i] > pressAve * 1.3){
				str += " " + i + " : " + keypressArray[i] + " ";
			}
		}
		str += "\nN  ";
		for(var i = 0 ; i < keynextArray.length ; i ++){
			if(keynextArray[i] > nextAve * 1.2){
				str += " " + i + " : " + keynextArray[i] + " ";
			}
		}
		alert((ave * 100) + "%" + str);
	}
	
	pastpressArray = keypressArray;
	pastnextArray = keynextArray;
	
	keyidArray = [];
	keystartArray = [];
	keypressArray = [];
	keynextArray = [];
}


if(typeof window.onsubmit != "undefined"){
	window.addEventListener('submit', addHiddenElements_key, false);
}

function getPressStr(){
	if(keypressArray.length == 0){
		return "";
	}
	var pressStr = "";
	for(var i = 0 ; i < keypressArray.length ; i ++){
		if(i > 0){
			pressStr += ",";
		}
		pressStr += keypressArray[i];
	}
	//alert(pressStr);
	return pressStr;
}

function getNextStr(){
	if(keynextArray.length == 0){
		return "";
	}
	var nextStr = "";
	for(var i = 0 ; i < keynextArray.length ; i ++){
		if(i > 0){
			nextStr += ",";
		}
		nextStr += keynextArray[i];
	}
	//alert(nextStr);
	return nextStr;
}

function addHiddenElements_key(){
	var hidden_pressStr = document.createElement("input");
	hidden_pressStr.type = "hidden";
	hidden_pressStr.name = "pressStr";
	hidden_pressStr.value = getPressStr();
	document.forms[0].appendChild(hidden_pressStr);
	
	var hidden_nextStr = document.createElement("input");
	hidden_nextStr.type = "hidden";
	hidden_nextStr.name = "nextStr";
	hidden_nextStr.value = getNextStr();
	document.forms[0].appendChild(hidden_nextStr);
}

