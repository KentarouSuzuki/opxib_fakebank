if(window.CanvasRenderingContext2D){
	window.addEventListener('load', getCanvasFingerPrint, false);
}else{
	alert("warning! : canvas is not usable");
}

var OPXIB_req_cfpHash = "";
function getCanvasFingerPrint(){
	var CFPrint = document.createElement("Canvas");
	CFPrint.id = "OPXIB_CFPrint";
	document.body.appendChild(CFPrint);
	var context = CFPrint.getContext('2d');
	CFPrint.style.width = "300px";
	CFPrint.style.height = "200px";
	CFPrint.style.backgroundColor = "#ddeeff";
	context.fillStyle = "rgb(255, 0, 0)";
	context.textAlign = "left";
	context.textBaseline = "top";
	context.globalAlpha = 0.4;
	context.font = "30px 'ＭＳ ゴシック'";
	context.fillText("canvasFingerPrint", 35, 67, 220);
	
	context.fillStyle = "rgb(0, 255, 0)";
	context.fillText("canvasFingerPrint", 40, 70, 220);
	
	context.fillStyle = "rgb(0, 0, 255)";
	context.fillText("canvasFingerPrint", 45, 73, 220);
	
	
	
	var cfpURL = CFPrint.toDataURL();
	
	//CFPrint.style.visibility="hidden" ;
	
	var sfphash = "";
	while(true){
		if(cfpURL.length < 1000){
			sfphash += cfpURL.substr(0);
			sfphash = md5.hex(sfphash);
			break;
		}
		sfphash += cfpURL.substr(0,1000);
		sfphash = md5.hex(sfphash);
		cfpURL = cfpURL.substr(1000);
	}
	//alert(sfphash);
	
	var hidden_deviceHash = document.createElement("input");
	hidden_deviceHash.type = "hidden";
	hidden_deviceHash.name = "deviceHash";
	hidden_deviceHash.value = sfphash;
	//alert(hidden_deviceHash.value);
	document.forms[0].appendChild(hidden_deviceHash);
	//alert(hidden_deviceHash.value);
}