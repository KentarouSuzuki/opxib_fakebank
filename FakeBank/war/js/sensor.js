var OPXIB_sensorValue_fin = 0;

var OPXIB_pastZ = 0;
var differenceArray = [];   // "センサ値同士の差"の配列
var minNearArray = [];      // "仮の最小刻み"の近似値の配列
var arrayCheckFlags = [];   // "センサ値同士の差の配列"から仮の最小刻みの近似値を検出する際、
	                        // その値がすでに確認済みかどうかを判断するためのフラグ
var OPXIB_count = 0;
var OPXIB_OPXIB_countMax = 10;

var OPXIB_runflag = false;

/*function getAccelerometerValue(){
	OPXIB_runflag = true;
	window.addEventListener('devicemotion', accelerometerEvent, false);
	while(OPXIB_runflag){
	}
}*/

if(typeof window.TouchEvent != "undefined"){
	if(window.addEventListener){
		window.addEventListener('devicemotion', accelerometerEvent, false);
		//alert("sensor start");
	}
}

if(typeof window.onsubmit){
	window.addEventListener('submit', addHiddenElements_accele, false);
	OPXIB_runflag = true;
}

function addHiddenElements_accele(){
	var hidden_accele = document.createElement("input");
	hidden_accele.type = "hidden";
	hidden_accele.name = "accelerometer";
	hidden_accele.value = OPXIB_sensorValue_fin;
	document.forms[0].appendChild(hidden_accele);

	//alert("sensor set");
}

function accelerometerEvent(e){
	if(!OPXIB_runflag){
		return;
	}
	
	var x = e.accelerationIncludingGravity.x;
	var y = e.accelerationIncludingGravity.y;
	var z = e.accelerationIncludingGravity.z;
	
	//OPXIB_debug_X.innerHTML = x;

	// 取得したセンサ値と直前のセンサ値との差を算出
	var dif = z - OPXIB_pastZ;
	OPXIB_pastZ = z;
	if(dif < 0) dif = dif * -1;
	if(dif == 0){
	// 差が０の場合ここで中断
		return;
	}

	// 求めた差を"センサ値同士の差の配列"に格納する
	var pushIndex = 0;
	for(var i = 0 ; i < differenceArray.length ; i ++){
		if(differenceArray[i] *0.9 < dif && differenceArray[i] *1.1 > dif){
		// すでに近似値が配列に格納されている
			pushIndex = -1;
			if(i == 0){
			// 仮の最小刻みの近似値であれば、"近似値の配列"に格納
				minNearArray.push(dif);
			}
			break;
		}else if(differenceArray[i] > dif){
		// 配列の途中に挿入位置を発見
			break;
		}
		pushIndex ++;
	}
	if(pushIndex == 0 && differenceArray.length == 0){
	// 配列が空の時
		differenceArray.push(dif);
	}else if(pushIndex >= 0){
		differenceArray.splice(pushIndex, 0, dif);
		arrayCheckFlags.splice(pushIndex, 0, false);
		if(pushIndex == 0){
		// 先頭に挿入された ＝ 仮の最小刻みが更新された場合
			// 近似値の配列を初期化
			minNearArray = [];
		}else{
			arrayCheckFlags[pushIndex -1] = false;
		}
		arrayCheckFlags[pushIndex +1] = false;
	}
			
	if(differenceArray.length == 0) return;
			
	// センサ値同士の差の関係から、より小さな仮の最小刻みを探す
	var min = differenceArray[0];
	for(var i = 0 ; i < differenceArray.length -1; i ++){
		var difference_diff = differenceArray[i+1] - differenceArray[i];
		var minFlag = false;
		if(difference_diff < min * 0.9){
		// 隣接した差同士の"差"が、仮の最小刻みよりも十分に小さい場合
		// 差同士の差を先頭に挿入し、新たな仮の最小刻みとする
			differenceArray.splice(0, 0, difference_diff);
			minFlag = true;
		}else{
			pushIndex = 0;
			for(var j = 0 ; j < differenceArray.length ; j ++){
				if(differenceArray[j] *0.9 < difference_diff &&
					differenceArray[j] *1.1 > difference_diff){
					// すでに近似値が配列に格納されている
						pushIndex = -1;
						break;
				}else if(differenceArray[j] > difference_diff){
				// 配列の途中に挿入位置を発見
					break;
				}
				pushIndex ++;
			}
			if(pushIndex == 0 && differenceArray.length == 0){
			// 配列が空の時
				differenceArray.push(difference_diff);
			}else if(pushIndex >= 0){
				differenceArray.splice(pushIndex, 0, difference_diff);
				arrayCheckFlags.splice(pushIndex, 0, false);
				if(pushIndex == 0){
				// 先頭に挿入された ＝ 仮の最小刻みが更新された場合
				// 近似値の配列を初期化
					minNearArray = [];
				}else{
					arrayCheckFlags[pushIndex -1] = false;
				}
				arrayCheckFlags[pushIndex +1] = false;
			}
		}
			
		if(minFlag){
			min = differenceArray[0];
			i = -1;
			OPXIB_count = 0;
			minNearArray = [];
			for(var j = 0 ; j < arrayCheckFlags.length ; j ++){
				arrayCheckFlags[j] = false;
			}
		}
	}
		
	// センサ値同士の差から、現在の仮の最小刻みの近似値が存在しているかチェックする
	for(var i = 0 ; i < differenceArray.length -1 ; i ++){
		if(arrayCheckFlags[i]) continue;
		var near = differenceArray[i+1] - differenceArray[i];
		if(min *0.9 < near && min *1.1 > near){
		// 近似値の場合
			minNearArray.push(near);
			arrayCheckFlags[i] = true;
		}
	}

	//sensorValue.innerHTML = "run";
	//sensorZ.innerHTML = z;
	createId(min);

	if(min >= 0.1){
		OPXIB_count += 0.5;
	}else{
		OPXIB_count ++;
	}
}

function createId(dif){
	if( (OPXIB_count >= OPXIB_OPXIB_countMax && minNearArray.length >= 7) || (OPXIB_count >= 20) ){
		createFlag = false;
				
		// 近似値を十分に収集できていれば、最小値と最大値を除外する
		if(minNearArray.length >= 5){
			var nearMinIndex = 0;
			var nearMaxIndex = 0;
			for(var i = 1 ; i < minNearArray.length ; i ++){
				if(minNearArray[i] < minNearArray[nearMinIndex]){
					nearMinIndex = i;
				}
				if(minNearArray[i] > minNearArray[nearMaxIndex]){
					nearMaxIndex = i;
				}
			}
			minNearArray.splice(nearMinIndex, 1);
			minNearArray.splice(nearMaxIndex, 1);
		}
			
		var dummyNear1 = differenceArray[0] * 1.000009;
		var dummyNear2 = differenceArray[0] * 0.999991;
		minNearArray.push(dummyNear1);
		minNearArray.push(dummyNear2);

	// 有効桁数を決定する
		var num = 10000000000; // 小数第１０位
		for(var i = 3 ; i < minNearArray.length ; i ++){
			var num1 = Math.floor(minNearArray[2] * num) / num;
			var num2 = Math.floor(minNearArray[i] * num) / num;
			if(num1 != num2){
				if(num <= 10){
					return;
				}else{
					i = 2;
					// 有効桁を１桁切り捨てる
					num /= 10;
				}
			}
		}
			
		//sensorValue.innerHTML = Math.floor(minNearArray[2] * num) / num;
		OPXIB_runflag = false;
		//OPXIB_debug_Y.innerHTML = minNearArray[2];
		OPXIB_sensorValue_fin = Math.floor(minNearArray[2] * num);
		//alert(OPXIB_sensorValue_fin);
		//window.removeEventListener('devicemotion', accelerometerEvent, false);
	}
}