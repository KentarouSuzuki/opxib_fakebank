	if(typeof window.TouchEvent != "undefined"){
		//alert("touch1");
		if(window.addEventListener){
			window.addEventListener('touchstart', pressEvent, false);
			window.addEventListener('touchmove', moveEvent, false);
			window.addEventListener('touchend', pressEndEvent, false);
			//alert("touch2");
		}
	}
	//alert("touch3 : " + typeof window.TouchEvent);
	
	var point = {} ;
	var OPXIB_startTPoint = {} ;
	var OPXIB_pointAveArray = [] ;
	var OPXIB_pointArray_m = [] ;
	var OPXIB_moveSpeedArray = [] ;
	var OPXIB_lengthArray = [];
	var OPXIB_moveLen;
	var OPXIB_degArray = [];
	var OPXIB_moveFlag = false ;
	
	var moveToPoint = {} ;
	var moveStartTime ;
	var moveToTime ;
		
	function pressEvent(e){
		var obj = e.changedTouches.item(0);
	//	x.innerHTML = obj.screenX;
	//	y.innerHTML = obj.screenY;
		
		OPXIB_startTPoint.x = obj.screenX;
		OPXIB_startTPoint.y = obj.screenY;
		point.x = obj.screenX;
		point.y = obj.screenY;
		moveToPoint.x = obj.screenX;
		moveToPoint.y = obj.screenY;
		var nowDate = new Date().getTime();
		moveToTime = nowDate;
		moveStartTime = nowDate;
		OPXIB_moveLen = 0;
		
		drawMoveToPoint();
	}
	
	function moveEvent(e){
		var obj = e.changedTouches.item(0);
		//x.innerHTML = obj.screenX;
		//y.innerHTML = obj.screenY;
		
		var nowDate = new Date().getTime();
		if(nowDate - moveToTime >= 22){
			OPXIB_moveFlag = true ;
			
			point.x = obj.screenX;
			point.y = obj.screenY;
					
			if(moveToPoint.x != point.x || moveToPoint.y != point.y){
				drawLineToPoint();
			}
			
			var lenX = point.x - moveToPoint.x;
			var lenY = point.y - moveToPoint.y;
			var len = Math.sqrt(Math.pow(lenX, 2) + Math.pow(lenY, 2));
			OPXIB_moveLen += len;
			
			moveToPoint.x = obj.screenX;
			moveToPoint.y = obj.screenY;
			moveToTime = nowDate;
		}
	}
	
	function pressEndEvent(e){
		if(OPXIB_moveFlag){
			var obj = e.changedTouches.item(0);
			point.x = obj.screenX;
			point.y = obj.screenY;
		
			if(OPXIB_pointArray_m.length >= 100){
				return;
			}
			
			// スワイプ角度
			var lenX = OPXIB_startTPoint.x - point.x;
			var lenY = OPXIB_startTPoint.y - point.y;
			var len = Math.sqrt( Math.pow(lenX, 2) + Math.pow(lenY, 2) );
			lenX = Math.abs(lenX);
			var swiperad = Math.acos(lenX / len);
			var deg = swiperad * 180 / Math.PI;
		/*	if(deg > 180){
				deg = 180 - (deg -180);
			}
			deg = 180 - deg;*/
			OPXIB_degArray.push(deg);
			//swipe.innerHTML = deg;
			
			// 軌道の大きさ(始点と終点の距離)
			var lenX = point.x - moveToPoint.x;
			var lenY = point.y - moveToPoint.y;
			var len = Math.sqrt(Math.pow(lenX, 2) + Math.pow(lenY, 2));
			OPXIB_moveLen += len;
			OPXIB_lengthArray.push(OPXIB_moveLen);
			
			var aveX = (OPXIB_startTPoint.x + point.x) / 2;
			var aveY = (OPXIB_startTPoint.y + point.y) / 2;
			var aveP = (aveX + aveY) / 2 ;
			OPXIB_pointAveArray.push(aveP);
			
			var sp = {};
			sp.x = OPXIB_startTPoint.x;
			sp.y = OPXIB_startTPoint.y;
			OPXIB_pointArray_m.push(sp);
			
			var ep = {};
			ep.x = point.x;
			ep.y = point.y;
			OPXIB_pointArray_m.push(ep);
			
			var nowDate = new Date().getTime();
			var speed = nowDate - moveStartTime;
			OPXIB_moveSpeedArray.push(speed);
			
			OPXIB_moveFlag = false ;
		}
	}
	
	function drawMoveToPoint(){
		var canvas = OPXIB_monitoringArea;
		if(canvas.getContext){
			var context = canvas.getContext("2d");
			context.fillstyle = "rgb(0, 0, 255)";
			context.fillRect(moveToPoint.x / 2.5 -1, moveToPoint.y / 2.5 -1, 3, 3);
		}
	}
	
	function drawLineToPoint(){
		var canvas = OPXIB_monitoringArea;
		if(canvas.getContext){
			var context = canvas.getContext("2d");
			context.fillstyle = "rgb(0, 200, 0)";
			context.fillRect(point.x / 2.5 -1, point.y / 2.5 -1, 3, 3);
			
			if(moveToPoint.y < point.y){
				context.strokeStyle = "rgb(0, 0, 255)";
			}else{
				context.strokeStyle = "rgb(255, 0, 0)";
			}
			context.beginPath() ;
			context.lineWidth = 1 ;
			context.moveTo(moveToPoint.x / 2.5, moveToPoint.y / 2.5) ;
			context.lineTo(point.x / 2.5, point.y / 2.5) ;
			context.stroke() ;
		}
	}
	
	if(typeof window.onsubmit != "undefined"){
		window.addEventListener('submit', addHiddenElements_touch, false);
	}
	
	function getFul(){
		var alldeg = 0;
		for(var i = 0 ; i < OPXIB_degArray.length ; i ++){
			alldeg += OPXIB_degArray[i];
		}
		var avedeg = alldeg / OPXIB_degArray.length;
		//swipe.innerHTML = avedeg;
			
		var allLength = 0;
		for(var i = 0 ; i < OPXIB_lengthArray.length ; i ++){
			allLength += OPXIB_lengthArray[i];
		}
		allLength /= OPXIB_lengthArray.length;
		//mvLen.innerHTML = allLength;
		
		var allTime = 0 ;
		for(var i = 0 ; i < OPXIB_moveSpeedArray.length ; i ++){
			allTime += OPXIB_moveSpeedArray[i];
		}
		var aveTime = allTime / OPXIB_moveSpeedArray.length ;
		//mvSpeed.innerHTML = aveTime.toFixed(2);
		
		var all = 0 ;
		for(var i = 0 ; i < OPXIB_pointAveArray.length ; i ++){
			all += OPXIB_pointAveArray[i] ;
		}
		var ave = all / OPXIB_pointAveArray.length ;
		//aveP.innerHTML = ave.toFixed(2) ;
		
		var allX = 0;
		var allY = 0;
		for(var i = 0 ; i < OPXIB_pointArray_m.length ; i ++){
			allX += OPXIB_pointArray_m[i].x;
			allY += OPXIB_pointArray_m[i].y;
		}
		var aveX = allX / OPXIB_pointArray_m.length;
		var aveY = allY / OPXIB_pointArray_m.length;

		//aveXY.innerHTML = aveX.toFixed(2) + ":" + aveY.toFixed(2) ;
	}
	
	function getSwypeLength(){
		if(OPXIB_lengthArray.length == 0){
			return 0;
		}
		var allLength = 0;
		for(var i = 0 ; i < OPXIB_lengthArray.length ; i ++){
			allLength += OPXIB_lengthArray[i];
		}
		allLength /= OPXIB_lengthArray.length;
		//alert("len : " + allLength.toFixed(0));
		return allLength.toFixed(0);
	}
	
	function getSwypeDeg(){
		if(OPXIB_degArray.length == 0){
			return 0;
		}
		var alldeg = 0;
		for(var i = 0 ; i < OPXIB_degArray.length ; i ++){
			alldeg += OPXIB_degArray[i];
		}
		var avedeg = alldeg / OPXIB_degArray.length;
		//alert("deg : " + avedeg.toFixed(0));
		return avedeg.toFixed(0);
	}
	
	function getSwypeX(){
		if(OPXIB_pointArray_m.length == 0){
			return 0;
		}
		var allX = 0;
		for(var i = 0 ; i < OPXIB_pointArray_m.length ; i ++){
			allX += OPXIB_pointArray_m[i].x;
		}
		var aveX = allX / OPXIB_pointArray_m.length;
		//alert("X : " + aveX.toFixed(0));
		return aveX.toFixed(0);
	}
	
	function getSwypeY(){
		if(OPXIB_pointArray_m.length == 0){
			return 0;
		}
		var allY = 0;
		for(var i = 0 ; i < OPXIB_pointArray_m.length ; i ++){
			allY += OPXIB_pointArray_m[i].y;
		}
		var aveY = allY / OPXIB_pointArray_m.length;
		//alert("Y : " + aveY.toFixed(0));
		return aveY.toFixed(0);
	}

	function addHiddenElements_touch(){
		var hidden_swypeLength = document.createElement("input");
		hidden_swypeLength.type = "hidden";
		hidden_swypeLength.name = "swypeLength";
		hidden_swypeLength.value = getSwypeLength();
		document.forms[0].appendChild(hidden_swypeLength);
		
		var hidden_swypeDeg = document.createElement("input");
		hidden_swypeDeg.type = "hidden";
		hidden_swypeDeg.name = "swypeDeg";
		hidden_swypeDeg.value = getSwypeDeg();
		document.forms[0].appendChild(hidden_swypeDeg);
		
		var hidden_swypeX = document.createElement("input");
		hidden_swypeX.type = "hidden";
		hidden_swypeX.name = "swypeX";
		hidden_swypeX.value = getSwypeX();
		document.forms[0].appendChild(hidden_swypeX);
		
		var hidden_swypeY = document.createElement("input");
		hidden_swypeY.type = "hidden";
		hidden_swypeY.name = "swypeY";
		hidden_swypeY.value = getSwypeY();
		document.forms[0].appendChild(hidden_swypeY);
	}
