if(window.CanvasRenderingContext2D){
	window.addEventListener('load', createMonitoringArea, false);
}else{
	alert("warning! : canvas is not usable");
}

var debugV;
//alert(typeof test + "\n" + typeof debugV);

function createMonitoringArea(){
	var debugDiv = document.createElement("div");
	debugDiv.id = "monitoringDiv";
	document.body.appendChild(debugDiv);
	debugDiv.style.backgroundColor = '#dddddd';
	debugDiv.style.position = "fixed";
	debugDiv.style.bottom = "20px";
	debugDiv.style.right = "20px";
	debugDiv.style.visibility = "hidden";
	
	var div_mvX = document.createElement("div");
	debugDiv.appendChild(div_mvX);
	div_mvX.innerHTML = "X:"
	var mvX = document.createElement("span");
	mvX.id = "OPXIB_debug_X";
	div_mvX.appendChild(mvX);
	mvX.innerHTML = "-";
	
	var div_mvY = document.createElement("div");
	debugDiv.appendChild(div_mvY);
	div_mvY.innerHTML = "Y:"
	var mvY = document.createElement("span");
	mvY.id = "OPXIB_debug_Y";
	div_mvY.appendChild(mvY);
	mvY.innerHTML = "-";
	
	var disper = document.createElement("div");
	disper.id = "dispersion";
	debugDiv.appendChild(disper);
	disper.innerHTML = "dispersion (please enter any key)";
	
	var div_wheel = document.createElement("div");
	debugDiv.appendChild(div_wheel);
	div_wheel.innerHTML = "wheel:"
	var wheel = document.createElement("span");
	wheel.id = "OPXIB_debug_wheel";
	div_wheel.appendChild(wheel);
	wheel.innerHTML = "-";
	
	var canvas = document.createElement("Canvas");
	canvas.id = "OPXIB_monitoringArea";
	debugDiv.appendChild(canvas);
	canvas.style.width = (screen.availWidth / 4) + "px";
	canvas.style.height = (screen.availHeight / 4) + "px";
	canvas.style.left = "10px";
	canvas.style.backgroundColor = '#ffffff';
}