if(typeof window.onclick != "undefined" && typeof window.TouchEvent == "undefined"){
	//alert("onclick : " + typeof window.onclick);
	window.addEventListener('click', setMouseMove, false);
}
if(typeof document.onmousemove != "undefined" && typeof window.TouchEvent == "undefined"){
	//alert("onmousemove : " + typeof window.onmousemove);
	document.addEventListener('mousemove', mouseMoveEvent, false);
}

window.onbeforeunload = function(e){
	//alert("beforeunload");
	//window.location.href = "login";
	//e.returnValue = "test";
}

//alert(typeof window.TouchEvent);

//alert("mouse start");

var run = false ;
function setMouseMove(){
	OPXIB_pastPoint.x = OPXIB_point.x ;
	OPXIB_pastPoint.y = OPXIB_point.y ;
	//pastMoveTime = moveTime ;
	//test.innerHTML = moveTime ;
	drawPastPoint();
	OPXIB_moveX_10countAll = 0;
	OPXIB_moveY_10countAll = 0;
	OPXIB_moveCount = 0;
	OPXIB_pastRad = -999;

	var p = {};
	p.x = OPXIB_point.x;
	p.y = OPXIB_point.y;
	OPXIB_pointArray.push(p);
};


var OPXIB_point = {};
var OPXIB_pastPoint = {};
var OPXIB_pointArray = [];
var OPXIB_first = true ;
var OPXIB_moveCount = 0;
var OPXIB_moveX_10countAll = 0;
var OPXIB_moveY_10countAll = 0;
var OPXIB_pastRad = -999;


function mouseMoveEvent(e){
	/*var moveTime = new Date().getTime() ;
	var moveTimeDif = moveTime - pastMoveTime ;*/
	
	OPXIB_point.x = e.screenX;
	OPXIB_point.y = e.screenY;
	
	OPXIB_debug_X.innerHTML = OPXIB_point.x;
	OPXIB_debug_Y.innerHTML = OPXIB_point.y;
	
	if(OPXIB_first){
		OPXIB_first = false ;
		OPXIB_pastPoint.x = OPXIB_point.x ;
		OPXIB_pastPoint.y = OPXIB_point.y ;

		drawPastPoint();
		OPXIB_debug_X_10countAll = 0;
		OPXIB_debug_Y_10countAll = 0;
		OPXIB_moveCount = 0;
		OPXIB_pastRad = -999;
		
		var p = {};
		p.x = OPXIB_point.x;
		p.y = OPXIB_point.y;
		OPXIB_pointArray.push(p);
		return;
	}
	var moveX = OPXIB_point.x - OPXIB_pastPoint.x;
	var moveY = OPXIB_point.y - OPXIB_pastPoint.y;
	
	OPXIB_moveX_10countAll += moveX;
	OPXIB_moveY_10countAll += moveY;
	OPXIB_moveCount ++;
	if(OPXIB_moveCount >= 10){
		var moveLength =
			Math.sqrt( (OPXIB_moveX_10countAll * OPXIB_moveX_10countAll) +
					(OPXIB_moveY_10countAll * OPXIB_moveY_10countAll) );
		if(moveLength < 5){
			OPXIB_moveX_10countAll = 0;
			OPXIB_moveY_10countAll = 0;
			OPXIB_moveCount = 0;
			
			return;
		}
			
		var deg = Math.acos(OPXIB_moveX_10countAll / moveLength) * 180 / Math.PI;
		if(OPXIB_moveY_10countAll > 0){
			deg = 180 + (180 - deg);
		}
		
		var radMax = OPXIB_pastRad + 12;
		var radMin = OPXIB_pastRad - 12;
			
		if( OPXIB_pastRad != -999 && (deg >= radMax || deg <= radMin) ){
			OPXIB_pastPoint.x = OPXIB_point.x ;
			OPXIB_pastPoint.y = OPXIB_point.y ;
			drawPastPoint();
			OPXIB_moveX_10countAll = 0;
			OPXIB_moveY_10countAll = 0;
			OPXIB_moveCount = 0;
			OPXIB_pastRad = -999;

			var p = {};
			p.x = OPXIB_point.x;
			p.y = OPXIB_point.y;
			OPXIB_pointArray.push(p);
			return;
		}
			
		OPXIB_moveX_10countAll = 0;
		OPXIB_moveY_10countAll = 0;
		OPXIB_moveCount = 0;
		OPXIB_pastRad = deg ;
	}
		
	OPXIB_pastPoint.x = OPXIB_point.x ;
	OPXIB_pastPoint.y = OPXIB_point.y ;
}

function drawPastPoint(){
	var canvas = OPXIB_monitoringArea;
	if(canvas.getContext){
		var context = canvas.getContext("2d");
		context.fillstyle = "rgb(0, 0, 0)";
		context.fillRect(OPXIB_pastPoint.x /4 -1, OPXIB_pastPoint.y /4 -1, 3, 3);
		
		if(OPXIB_pointArray.length > 0){
			var edX = OPXIB_pointArray[OPXIB_pointArray.length -1].x ;
			var edY = OPXIB_pointArray[OPXIB_pointArray.length -1].y ;
			context.beginPath() ;
			context.lineWidth = 1 ;
			context.moveTo(edX /4, edY /4) ;
			context.lineTo(OPXIB_pastPoint.x /4, OPXIB_point.y /4) ;
			context.stroke();
		}
	}
}

var OPXIB_FulX, OPXIB_FulY;

function getDispersion(){
	if(OPXIB_pointArray.length < 3){
		return 0;
	}
	
	var startPoint = {};
	startPoint = OPXIB_pointArray[0];
	var endPoint = {};
	endPoint = OPXIB_pointArray[OPXIB_pointArray.length -1];
	
	var AB = {};
	AB.x = endPoint.x - startPoint.x;
	AB.y = endPoint.y - startPoint.y;
	
	var all = 0;
	var count = 0;
	
	for(var i = 1 ; i < OPXIB_pointArray.length -1 ; i ++){
		var AP = {};
		var p = {};
		p = OPXIB_pointArray[i];
		AP.x = p.x - startPoint.x;
		AP.y = p.y - startPoint.y;
		
		var D = AB.x * AP.y - AB.y * AP.x;
		var L = Math.sqrt( Math.pow(endPoint.x - startPoint.x, 2) + Math.pow(endPoint.y - startPoint.y, 2) );
		var H = D / L;
		
		all += H;
		count ++;
	}
	var ave = all / count;
	//alert(ave);
	return ave.toFixed(0);
}

/*function getFul(){
	// 平均
	var N = OPXIB_pointArray.length;
	var xcenter = screen.width / 2;
	var ycenter = screen.height / 2;
	var avex = 0 ;
	var avey = 0 ;
	for(var i = 0 ; i < N ; i ++){
		avex += OPXIB_pointArray[i].x ;//- xcenter;
		avey += OPXIB_pointArray[i].y ;//- ycenter;
	}
	avex /= N ;
	avey /= N ;
	
	// 偏差
	var dev = {} ;
	var devArray = [] ;
	
	for(var i = 0 ; i < N ; i ++){
		dev.x = OPXIB_pointArray[i].x - avex ;
		dev.y = OPXIB_pointArray[i].y - avey ;
		devArray.push(dev) ;
	}
	
	// 偏差平方和
	var devSqx = 0 ;
	var devSqy = 0 ;
	
	for(var i = 0 ; i < N ; i ++){
		devSqx += devArray[i].x * devArray[i].x ;
		devSqy += devArray[i].y * devArray[i].y ;
	}

	// 分散
	var Vx = devSqx / (N -1) ;
	var Vy = devSqy / (N -1) ;
	
	// 標準偏差
	var stVx = Math.sqrt(Vx) ;
	var stVy = Math.sqrt(Vy) ;
//	bunpu.innerHTML = "test";
	//変動係数
	var Fulx = (stVx / avex * 100);
	var Fuly = (stVy / avey * 100);
	
	OPXIB_FulX = Fulx;
	OPXIB_FulY = Fuly;
}*/


//document.onkeydown = function(e){
if(typeof window.onkeydown && typeof window.TouchEvent == "undefined"){
	//alert("onkeydown : " + typeof window.onkeydown);
	window.addEventListener('keydown', keydownEvent, false);
}

function keydownEvent(e){
	if(OPXIB_pointArray.length == 0){
		return;
	}
	drawPastPoint();
	var p = {};
	p.x = OPXIB_point.x;
	p.y = OPXIB_point.y;
	OPXIB_pointArray.push(p);
	getFul();
	dispersion.innerHTML = OPXIB_FulX.toFixed(2) + ", " + OPXIB_FulY.toFixed(2);
	//location.href += "?";
}

if(!window.addEventListener){
	alert("IE");
}else if(typeof window.onmousewheel === "undefined"){
	window.addEventListener('DOMMouseScroll', mouseScrollEvent, false);
}else if(typeof window.TouchEvent == "undefined"){
	window.addEventListener('mousewheel', mouseWheelEvent, false);
	//window.onmousewheel = mouseWheelEvent;
}

var wheelcount = 0;
var OPXIB_wheelmin = 0;

var OPXIB_wheel = 0;
var OPXIB_wheelArray = [];
var OPXIB_wheelDate = 0;
var OPXIB_wheelIntervalArray = [];

function mouseWheelEvent(e){
	if(OPXIB_wheelIntervalArray.length < 30){
		var nowDate = new Date().getTime();
		var interval = nowDate - OPXIB_wheelDate;
		if(interval > 50 && interval < 1000){
			OPXIB_wheelArray.push(OPXIB_wheel);
			OPXIB_wheelIntervalArray.push(interval);
			OPXIB_wheel = 0;
		}
		OPXIB_wheelDate = nowDate;
		OPXIB_wheel += e.wheelDelta;
	}
	
	var wheelAbs = Math.abs(e.wheelDelta);
	if(wheelAbs < OPXIB_wheelmin || OPXIB_wheelmin == 0){
		OPXIB_wheelmin = wheelAbs;
	}
	wheelcount += e.wheelDelta;
	OPXIB_debug_wheel.innerHTML = wheelcount + " : " + OPXIB_wheelmin;
}

function mouseScrollEvent(e){
	if(OPXIB_wheelIntervalArray.length < 30){
		var nowDate = new Date().getTime();
		var interval = nowDate - OPXIB_wheelDate;
		if(interval > 50 && interval < 1000){
			OPXIB_wheelArray.push(OPXIB_wheel);
			OPXIB_wheelIntervalArray.push(interval);
			OPXIB_wheel = 0;
		}
		OPXIB_wheelDate = nowDate;
		OPXIB_wheel += e.detail;
	}
	
	var wheelAbs = Math.abs(e.detail);
	if(wheelAbs < OPXIB_wheelmin || OPXIB_wheelmin == 0){
		OPXIB_wheelmin = wheelAbs;
	}
	wheelcount += e.detail;
	OPXIB_debug_wheel.innerHTML = wheelcount + " : " + OPXIB_wheelmin;
}

if(typeof window.onsubmit != "undefined"){
	window.addEventListener('submit', addHiddenElements_mouse, false);
}

function getWheelIntervalAverage(){
	if(OPXIB_wheelIntervalArray.length == 0){
		return 0;
	}
	
	var all = 0;
	for(var i = 0 ; i < OPXIB_wheelIntervalArray.length ; i ++){
		all += OPXIB_wheelIntervalArray[i];
	}
	var ave = all / OPXIB_wheelIntervalArray.length;
	return ave.toFixed(0);
}

function getWheelAverage(){
	if(OPXIB_wheelArray.length == 0){
		return 0;
	}
	
	var all = 0;
	for(var i = 0 ; i < OPXIB_wheelArray.length ; i ++){
		all += OPXIB_wheelArray[i];
	}
	var ave = all / OPXIB_wheelArray.length;
	return ave.toFixed(0);
}

function addHiddenElements_mouse(){
	alert(document.forms.length);
	
	var hidden_mouseDispersion = document.createElement("input");
	hidden_mouseDispersion.type = "hidden";
	hidden_mouseDispersion.name = "mouseDispersion";
	hidden_mouseDispersion.value = getDispersion();
	document.forms[0].appendChild(hidden_mouseDispersion);
	
	var hidden_wheel = document.createElement("input");
	hidden_wheel.type = "hidden";
	hidden_wheel.name = "wheel";
	hidden_wheel.value = getWheelAverage();
	document.forms[0].appendChild(hidden_wheel);
	
	var hidden_wheelInterval = document.createElement("input");
	hidden_wheelInterval.type = "hidden";
	hidden_wheelInterval.name = "wheelInterval";
	hidden_wheelInterval.value = getWheelIntervalAverage();
	document.forms[0].appendChild(hidden_wheelInterval);
	
	var hidden_wheelmin = document.createElement("input");
	hidden_wheelmin.type = "hidden";
	hidden_wheelmin.name = "wheelMin";
	hidden_wheelmin.value = OPXIB_wheelmin;
	document.forms[0].appendChild(hidden_wheelmin);
	
	//alert(OPXIB_wheelIntervalArray.length + " : " + getWheelIntervalAverage());
}

