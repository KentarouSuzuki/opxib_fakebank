package jp.co.cacco.fakebank.controller;

import org.slim3.tester.ControllerTestCase;

import jp.co.cacco.fakebank.controller.TransferController;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class TransferControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.start("/transfer");
        TransferController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is("/transfer.jsp"));
    }
}
