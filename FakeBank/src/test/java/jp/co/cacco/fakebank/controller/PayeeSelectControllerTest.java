package jp.co.cacco.fakebank.controller;

import org.slim3.tester.ControllerTestCase;

import jp.co.cacco.fakebank.controller.PayeeSelectController;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PayeeSelectControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.start("/payeeSelect");
        PayeeSelectController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is("/payeeSelect.jsp"));
    }
}
