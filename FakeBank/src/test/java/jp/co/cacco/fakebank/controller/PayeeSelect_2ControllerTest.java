package jp.co.cacco.fakebank.controller;

import org.slim3.tester.ControllerTestCase;

import jp.co.cacco.fakebank.controller.PayeeSelect_2Controller;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PayeeSelect_2ControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.start("/payeeSelect_2");
        PayeeSelect_2Controller controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is("/payeeSelect_2.jsp"));
    }
}
