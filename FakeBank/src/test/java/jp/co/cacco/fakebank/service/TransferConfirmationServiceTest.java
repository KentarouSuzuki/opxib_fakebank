package jp.co.cacco.fakebank.service;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class TransferConfirmationServiceTest extends AppEngineTestCase {

    private TransferConfirmationService service = new TransferConfirmationService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
