package jp.co.cacco.fakebank.service;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PayeeSelect_2ServiceTest extends AppEngineTestCase {

    private PayeeSelect_2Service service = new PayeeSelect_2Service();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
