package jp.co.cacco.fakebank.controller;

import javax.servlet.http.HttpSession;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import jp.co.cacco.fakebank.service.PayeeSelect_2Service;

public class PayeeSelect_2Controller extends Controller {
    PayeeSelect_2Service service = new PayeeSelect_2Service();
    
    @Override
    public Navigation run() throws Exception {
        HttpSession session = request.getSession(false);
        if(session == null){
            return redirect("/");
        }
        if(isPost()){
            
            int mouseDispersion = Integer.valueOf(request.getParameter("mouseDispersion"));
            System.out.println(mouseDispersion);
            
            int md = (int) session.getAttribute("mouseDispersion_login");
            System.out.println(md);
            session.setAttribute("mouseDispersion_payeeS2", mouseDispersion);
            
            // API
            //boolean result = service.callAPI(request, response);
            return redirect("TransferDetail");
        }else{
            return forward("payeeSelect_2.html");
        }
    }
}
