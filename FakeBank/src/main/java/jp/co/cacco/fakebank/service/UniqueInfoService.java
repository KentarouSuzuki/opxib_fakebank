package jp.co.cacco.fakebank.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

public class UniqueInfoService {
    public String sendAPI(HttpServletRequest request){
        try{
            URL url = new URL("http://52.68.122.147:8080/v1/event");
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("User-Agent", "@UniqueInfo URLConnection");
            urlConnection.setRequestProperty("Accept-Language", "ja");
            
            String param = "loginId=" + request.getParameter("loginId") + "&" +
                    "mouseDispersion=" + request.getParameter("mouseDispersion") + "&" +
                    "wheel=" + request.getParameter("wheel") + "&" +
                    "wheelInterval=" + request.getParameter("wheelInterval") + "&" +
                    "wheelMin=" + request.getParameter("wheelMin") + "&" +
                    "deviceHash=" + request.getParameter("deviceHash") + "&" +
                    "accelerometer=" + request.getParameter("accelerometer") + "&" +
                    "swypeX=" + request.getParameter("swypeX") + "&" +
                    "swypeY=" + request.getParameter("swypeY") + "&" +
                    "swypeLength=" + request.getParameter("swypeLength") + "&" +
                    "swypeDeg=" + request.getParameter("swypeDeg") + "&" +
                    "pressStr=" + request.getParameter("pressStr") + "&" +
                    "nextStr=" + request.getParameter("nextStr");
           System.out.print(request.getParameter("accelerometer"));
           // String param = "loginId=baduser&description=fake";
            
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            
            out.write(param);
            out.close();
           
            InputStream in = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder buf = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null){
                buf.append(line);
            }
            
            System.out.println(buf.toString());
            reader.close();
        }catch(Exception e){
            System.out.println("error! :" + e);
        }
        return "";
    }
}
