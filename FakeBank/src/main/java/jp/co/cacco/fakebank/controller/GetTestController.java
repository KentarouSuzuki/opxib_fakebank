package jp.co.cacco.fakebank.controller;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import jp.co.cacco.fakebank.service.GetTestService;

public class GetTestController extends Controller {

    GetTestService service = new GetTestService();
    @Override
    public Navigation run() throws Exception {
        if(isGet()){
            String req_id = request.getParameter("id");
            if(req_id != null && req_id.length() > 0){
                int id = Integer.valueOf(req_id);
                String result = service.getEvent(id);
                System.out.println(result);
                return null;
            }else{
                return forward("getTest.html");
            }
        }else{
            return forward("getTest.html");
        }
    }
}
