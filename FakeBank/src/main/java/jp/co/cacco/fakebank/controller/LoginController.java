package jp.co.cacco.fakebank.controller;

import javax.servlet.http.HttpSession;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import jp.co.cacco.fakebank.service.LoginService;

public class LoginController extends Controller {
    private LoginService service = new LoginService();

    @Override
    public Navigation run() throws Exception {
        if(isPost()){
            
            int mouseDispersion = Integer.valueOf(request.getParameter("mouseDispersion"));
            System.out.println(mouseDispersion);
            
            // API
            //boolean result = service.callAPI(request, response);
             HttpSession session = request.getSession(false);
             if(session == null){
                 session = request.getSession(true);
             }
             session.setAttribute("mouseDispersion_login", mouseDispersion);
            return redirect("menu");
        }else{
            return forward("login.html");
        }
    }
}
