package jp.co.cacco.fakebank.controller;

import javax.servlet.http.HttpSession;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import jp.co.cacco.fakebank.service.TransferDetailService;

public class TransferDetailController extends Controller {
    private TransferDetailService service = new TransferDetailService();

    @Override
    public Navigation run() throws Exception {
        HttpSession session = request.getSession(false);
        if(session == null){
            return redirect("/");
        }
        if(isPost()){
            
            int mouseDispersion = Integer.valueOf(request.getParameter("mouseDispersion"));
            System.out.println(mouseDispersion);
            
            int md_l = (int) session.getAttribute("mouseDispersion_login");
            System.out.println(md_l);
            int md_d2 = (int) session.getAttribute("mouseDispersion_payeeS2");
            System.out.println(md_d2);
            
            session.setAttribute("mouseDispersion_detail", mouseDispersion);
            
            // API
            //boolean result = service.callAPI(request, response);
            return redirect("TransferConfirmation");
        }else{
            return forward("transferDetail.html");
        }
    }
}
