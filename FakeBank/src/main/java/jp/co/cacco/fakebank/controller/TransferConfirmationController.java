package jp.co.cacco.fakebank.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import jp.co.cacco.fakebank.service.TransferConfirmationService;

public class TransferConfirmationController extends Controller {
    TransferConfirmationService service = new TransferConfirmationService();

    @Override
    public Navigation run() throws Exception {
        if(isPost()){

            double sensor = Double.valueOf(request.getParameter("OPXIB_sensor"));
            int mouse = Integer.valueOf(request.getParameter("OPXIB_mouse"));
            int typing = Integer.valueOf(request.getParameter("OPXIB_typing"));
            System.out.println(sensor + " : " + mouse + " : " + typing);
            
            
            boolean result = service.callAPI(request, response);
            return redirect("/");
        }else{
            return forward("transferConfirmation.html");
        }
    }
}
