package jp.co.cacco.fakebank.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * ログイン画面でのAPI呼び出し
 */

public class LoginService {
    public boolean callAPI(HttpServletRequest request, HttpServletResponse response){
        try{
            URL url = new URL("http://52.68.122.147:8080/v1/event");
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("User-Agent", "@IT java-tips URLConnection");
            urlConnection.setRequestProperty("Accept-Language", "ja");
            
            String param = "loginId=baduser&description=fake";
            
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            
            out.write(param);
            out.close();
           
            InputStream in = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder buf = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null){
                buf.append(line);
            }
            
            System.out.println(buf.toString());
            reader.close();
        }catch(Exception e){
            System.out.println("error! :" + e);
        }
        return true;
    }
}
