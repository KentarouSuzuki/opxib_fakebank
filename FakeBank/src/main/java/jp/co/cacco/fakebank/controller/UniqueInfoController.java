package jp.co.cacco.fakebank.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import jp.co.cacco.fakebank.service.UniqueInfoService;

public class UniqueInfoController extends Controller {
    UniqueInfoService service = new UniqueInfoService();
    @Override
    public Navigation run() throws Exception {
        if(isPost()){
            service.sendAPI(request);
            return forward("unique_login.html");
        }else{
            return forward("unique_login.html");
        }
    }
}
