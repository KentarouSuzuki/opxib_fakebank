package jp.co.cacco.fakebank.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import jp.co.cacco.fakebank.service.TransferService;

public class TransferController extends Controller {
    TransferService service = new TransferService();
    
    @Override
    public Navigation run() throws Exception {
        if(isPost()){
            boolean result = service.callAPI(request, response);
            return redirect("payeeSelect");
        }else{
            return forward("transfer.html");
        }
    }
}
