package jp.co.cacco.fakebank.controller;

import javax.servlet.http.HttpSession;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class MenuController extends Controller {

    @Override
    public Navigation run() throws Exception {
        HttpSession session = request.getSession(false);
        if(session == null){
            return redirect("/");
        }
        
        return forward("menu.html");
    }
}
